import {
    SEARCH_ADVISOR,
    CLEAN_ADVISOR,
    FILTER_ADVISOR,
    CLEAN_FILTER_ADVISOR
} from '../constants/Types';

//all states this reducer contains.
const INITIAL_STATE = {
    search: '',
    password: '',
    user: null,
    error: '',
    loading: false,
    filter: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SEARCH_ADVISOR:
            return { ...state, search: action.payload };

        case CLEAN_ADVISOR:
            return { ...state, search: '' };

        case FILTER_ADVISOR:
            return { ...state, loading: true, error: '' };

        case CLEAN_FILTER_ADVISOR:
            return { ...state, ...INITIAL_STATE, user: action.payload };

        default:
            return state;
    }
};