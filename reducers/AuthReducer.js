import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER,
  LOGIN_USERS_SUCCESS,
  LOGIN_USERS_FAIL,
  FB_LOGIN,
  FB_LOGIN_SUCCESS,
  FB_LOGIN_FAIL,
  GOOGLE_LOGIN,
  GOOGLE_LOGIN_SUCCESS,
  GOOGLE_LOGIN_FAIL } from '../actions/types';

//all states this reducer contains.
const INITIAL_STATE = {
  email: '',
  password: '',
  user: null,
  error: '',
  loading: false,
  token: null
 };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
/// EMAIL AND PASSWORD LOGIN
    case EMAIL_CHANGED:
    return { ...state, email: action.payload };

    case PASSWORD_CHANGED:
    return { ...state, password: action.payload };

    //controlls any action i want to perform before starting to log the user
    case LOGIN_USER:
    return { ...state, loading: true, error: '' };

//...INITIAL_STATE takes all the defaults and user: overrides the initial value
    case LOGIN_USERS_SUCCESS:
    return { ...state, ...INITIAL_STATE, user: action.payload };

    case LOGIN_USERS_FAIL:
    return { ...state,
      error: 'Authentication Failed.',
      password: '',
      loading: false };
    //FACEBOOK LOGIN
    case FB_LOGIN:
    return { ...state, loading: true, error: '' };

    case FB_LOGIN_SUCCESS:
    return { ...state, ...INITIAL_STATE, token: action.payload };

    case FB_LOGIN_FAIL:

    console.log('faul fb');
    return { ...state,
      error: 'Authentication Failed.',
      password: '',
      loading: false,
      token: null
     };
    // GOOGLE LOGIN
     case GOOGLE_LOGIN:
     return { ...state, loading: true, error: '' };

     case GOOGLE_LOGIN_SUCCESS:
     return { ...state, ...INITIAL_STATE, token: action.payload };

     case GOOGLE_LOGIN_FAIL:
     return { ...state,
       error: 'Authentication Failed.',
       password: '',
       loading: false,
       token: null
      };

    default:
    return state;
  }
};
