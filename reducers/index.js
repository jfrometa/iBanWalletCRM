import { combineReducers } from 'redux';
import AdvisorReducer from './AdvisorReducer';
import AuthReducer from './AuthReducer'

export default combineReducers({
    advisorReducer: AdvisorReducer,
    auth: AuthReducer
});
