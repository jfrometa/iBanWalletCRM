const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  backGroundDarkGray: "#ededed",
  foreGroundDarkGray: '#f6f6f6',
  textColor: 'black',
  iconColor: 'black',
  ibanOnlineGreen: '#339966',
  ibanOnlineDarkGreen: '#3d4324'
};
