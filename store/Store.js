import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { AsyncStorage } from 'react-native';
import reducers from '../reducers';

const store = createStore(
    reducers,
    {}
);

// persistStore(
//     store,
//     {
//         storage: AsyncStorage,
//         whitelist: [
//             'advisorReducer',]
//     }).purge();

export default store;
