import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import ClientCard from '../components/ClientCard';

export default class LinksScreen extends React.Component {
  static navigationOptions = {
    title: 'Links',
  };

  renderClientDetails(){
    return(
      < View style = { styles.cardContainer } >
        <View style={styles.cardColumnContainer}>

          <View style={styles.cardColumn}>
            <Ionicons style={styles.icon} name={"ios-mail"} size={18} color="black" />
            <Text style={styles.textStyle}>jfrometa@gmail.com</Text>
          </View>

          <View style={styles.cardColumn}>
            <Ionicons style={styles.icon} name={"ios-calendar"} size={18} color="black" />
            <Text style={styles.textStyle}>02/07/2019</Text>
          </View>

          <View style={styles.cardColumn}>
            <Ionicons style={styles.icon} name={"ios-stats"} size={18} color="black" />
            <Text style={styles.textStyle}>{"RESTRICTED_INVESTOR".replace("_", " ")}</Text>
          </View>
        </View>

        <View style={styles.cardColumnContainer}>

          <View style={styles.cardColumn}>
            <Ionicons style={styles.icon} name={"ios-phone-portrait"} size={18} color="black" />
            <Text style={styles.textStyle}>+11 (55) 1529-2125</Text>
          </View>

          <View style={styles.cardColumn}>
            <Text style={styles.textStyle}>Opciones: </Text>
            <Ionicons style={styles.icon} name={"ios-eye"} size={24} color="black" />
          </View>

        </View>

      </View >
      );
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <ClientCard title="A Panel with short content text" >
            {this.renderClientDetails()}
        </ClientCard>
        <ClientCard title="Text Title 1">
          <Text>Lorem ipsum...</Text>
        </ClientCard>
        <ClientCard title="Another Panel">
          {this.renderClientDetails()}
        </ClientCard>
      </ScrollView>
    )}
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f7f9',
    paddingTop: 30
  },
  cardContainer:{
    flexDirection: 'row',
    marginBottom: 2
  },
  cardColumnContainer:{
    flexDirection: 'column',
    justifyContent: 'center',
  },
  cardColumn: {
    flexDirection: 'row',
    margin: 5,
    marginBottom: 0
  },
  icon :{
    padding: 2
  },
  cardText: {
    padding: 1
  },
  textStyle:{
    margin: 2,
    marginStart: 4,
    marginBottom: 0
  }
});
