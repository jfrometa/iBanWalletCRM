import _ from 'lodash';
import React, { Component } from 'react';
import {
    ListView,
    View,
    Text,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableHighlight,
    Dimensions,
} from 'react-native';
import { Ionicons, MaterialIcons, SimpleLineIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { SearchBar } from 'react-native-elements'
import Communications from 'react-native-communications';
import ClientCard from '../components/ClientCard';
import GeneralInfo from '../components/GeneralInfo'
import SearchAndFilter from '../components/SearchAndFilter'
import Colors from '../constants/Colors';
import {
    findAdvisor,
    cleanAdvisor,
    filterAdvisors,
    cleanFilterAdvisors
} from '../actions'

const styles = StyleSheet.create({
    screenContainer: {
        flex: 1
    },
    textStyle: {
        fontSize: 15,
        paddingLeft: 8,
        margin: 2,
        marginStart: 4,
        marginBottom: 0
    },
    searchContainerStyle: {
        backgroundColor: 'white',
        borderTopWidth: 0,
        borderBottomWidth: 0
    },
    searchInputStyle: {
        color: 'black',
        borderRadius: 6
    },
    separator: {
        marginBottom: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#dcdcdc'
    }, cardContainer: {
        backgroundColor: Colors.foreGroundDarkGray,
        flexDirection: 'row',
    },
    cardColumnContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    cardColumn: {
        flexDirection: 'row',
        margin: 6,
        marginBottom: 0
    },
    icon: {
        padding: 2,
        marginLeft: 13

    },
    searchIcon: {
        marginRight: 5
    },
    cardText: {
        padding: 1
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    }
});

class ClientListScreen extends Component {
    static navigationOptions = {
        title: 'Asesores',
    };

    constructor(props) {
        super(props);

    }

    componentWillMount() {

        this.createDataSource(this.dummyData);
    }

    createDataSource() {
        dummyData = [
            "title 0", "title 1", "title 2", "title 3", "title 4", "title 6", "title 7",
            "title 8", "title 9", "title 10", "title 11", "title 12", "title 13"
        ];
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(dummyData);
    }

    makeCall() {
        Communications.phonecall('8099880275', true)
    }

    sendMail() {
        Communications.email(['josefrometa@ibanwallet.com', 'franciscohernandez@myibanwallet.com'], null, null, 'Titulo', 'Cuerpo del email')
    }

    renderClientDetails() {
        const { navigate } = this.props.navigation;
        return (
            < View style={styles.cardContainer} >
                <View style={styles.cardColumnContainer}>
                    <View style={styles.cardColumn}>
                        <Ionicons style={styles.icon} name={"ios-mail"} size={18} color={Colors.ibanOnlineGreen} />

                        <TouchableWithoutFeedback
                            style={[styles.shadow,
                            {
                                flexDirection: 'column', alignItems: 'center',
                                left: 9
                            }]}
                            onPress={this.sendMail.bind(this)}
                            underlayColor='rgba(0, 0, 0, .29)'>
                            <View>
                                <Text style={[styles.textStyle, { color: 'blue', textDecorationLine: 'underline' }]}>jfrometa@gmail.com</Text>
                            </View>

                        </TouchableWithoutFeedback>
                    </View>
                    <View style={styles.cardColumn}>

                        <Ionicons style={styles.icon}
                            name={"ios-phone-portrait"} size={18} color={Colors.ibanOnlineGreen} />

                        <TouchableWithoutFeedback
                            style={[styles.shadow,
                            {
                                flexDirection: 'column', alignItems: 'center',
                                left: 9
                            }]}
                            onPress={this.makeCall.bind(this)}>
                            <View>
                                <Text style={[styles.textStyle, { color: 'blue', textDecorationLine: 'underline' }]}>+1 809 988 0275</Text>
                            </View>

                        </TouchableWithoutFeedback>
                    </View>
                    <View style={styles.cardColumn}>
                        <Ionicons style={styles.icon} name={"ios-stats"} size={18} color={Colors.ibanOnlineGreen} />
                        <Text style={styles.textStyle}>{"Restricted_Investor"
                            .replace("_", " ")}</Text>
                    </View>
                </View>

                <View style={styles.cardColumnContainer}>

                    <View style={styles.cardColumn}>
                        <Ionicons style={styles.icon} name={"ios-calendar"} size={18} color={Colors.ibanOnlineGreen} />
                        <Text style={styles.textStyle}>02/07/2019</Text>
                    </View>

                    <View style={{ marginTop: 10 }}>
                        <TouchableHighlight
                            style={[styles.shadow,
                            {
                                flexDirection: 'column', alignItems: 'center',
                                left: 9, backgroundColor: Colors.ibanOnlineGreen,
                            }]}
                            onPress={() => navigate("Details")}
                            underlayColor='rgba(0, 0, 0, .29)'>

                            <SimpleLineIcons name={"list"}
                                size={25} color={'white'}
                            />

                        </TouchableHighlight>
                    </View>
                </View>
            </View >
        );
    }

    renderRow(client) {
        return (
            <ClientCard
                style={{ backgroundColor: Colors.foreGroundDarkGray }}
                title={client.title}>
                {this.renderClientDetails()}
            </ClientCard>
        );
    }

    render() {
        //get the screen width
        let deviceWidth = Dimensions.get('window').width;
        return (
            <View style={styles.screenContainer} >
                <SearchAndFilter />
                <ScrollView>
                    <GeneralInfo />
                    <ListView
                        enableEmptySections
                        dataSource={this.dataSource}
                        renderRow={this.renderRow.bind(this)}
                    // renderSeparator={(sectionID, rowID) =>
                    //      <View key={`${sectionID}-${rowID}`} style={styles.separator} />
                    //  }
                    />
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return state
}

export default connect(mapStateToProps,
    {
        findAdvisor,
        cleanAdvisor,
        filterAdvisors,
        cleanFilterAdvisors
    })(ClientListScreen)

