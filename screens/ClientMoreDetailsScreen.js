import _ from 'lodash';
import React, { Component } from 'react';
import {
    ListView,
    View,
    Text,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
} from 'react-native';
import { Ionicons, EvilIcons, Entypo } from '@expo/vector-icons';
import { connect } from 'react-redux';
import Colors from '../constants/Colors';

export default class ClientMoreDetailsScreen extends Component {
    static navigationOptions = {
        title: 'Otros Detalles',
    };

    constructor() {
        super();
    }

    componentWillMount() {
    }

    renderHeader() {
        return (
            <View style={{ backgroundColor: 'white', paddingTop: 8, paddingBottom: 8 }}>
                <View style={{ flexDirection: 'column' }}>
                    <Text
                        style={[styles.headerTextStyle, {
                            textAlign: 'center',
                            fontSize: 20,
                            fontWeight: '500',
                            marginTop: 42
                        }]}>
                        Jose Joaquin Frometa Guerra
                    </Text>
                    <Text
                        style={[styles.headerTextStyle,
                        {
                            textAlign: 'center',
                            fontSize: 14,
                            color: 'gray'
                        }]}
                    >CLV507536</Text>
                </View>
            </View>
        );

    }

    getTextWidth(substract) {
        // get the dimensions for diferent phone sizes
        let deviceWidth = Dimensions.get('window').width;
        return (deviceWidth / 2) - substract
    }

    renderClientDetails() {
        return (
            < View key="sticky-header" style={styles.cardContainerVertical} >
                <View style={[styles.fieldView, { marginHorizontal: 5 }]}>
                    <View style={[styles.cardColumn, { marginTop: 20 }]}>
                        <Text style={[
                            styles.textStyle,
                            { width: this.getTextWidth(20) }]}>
                            4 Rue des Bernadines
                        </Text>
                    </View>
                    <View style={styles.hintLine} />
                    <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Direccion</Text>
                </View>
                < View style={styles.cardContainer} >
                    <View style={styles.cardColumnContainer}>

                        <View style={[styles.fieldView, { marginVertical: 5 }]}>
                            <View style={styles.cardColumn}>
                                <Text style={[
                                    styles.textStyle,
                                    { width: this.getTextWidth(20) }]}>
                                    Tart l'Abbaye / FR
                                </Text>
                            </View>
                            <View style={styles.hintLine} />
                            <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Poblacion</Text>
                        </View>

                        <View style={[styles.fieldView, { marginVertical: 5 }]}>
                            <View style={styles.cardColumn}>
                                <Text style={[
                                    styles.textStyle,
                                    { width: this.getTextWidth(20) }]}>
                                    +1 809 988 - 0271
                                </Text>
                            </View>
                            <View style={styles.hintLine} />
                            <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Telefono</Text>
                        </View>
                    </View>
                    <View style={styles.cardColumnContainer}>
                        <View style={[styles.fieldView, { marginVertical: 5 }]}>
                            <View style={styles.cardColumn}>
                                <Text style={[
                                    styles.textStyle,
                                    {
                                        width: this.getTextWidth(20)
                                    }]}>21110
                            </Text>
                            </View>
                            <View style={styles.hintLine} />
                            <Text style={[styles.hintTextStyle,
                            styles.bottomTextStyle]}>Codigo Postal
                                </Text>
                        </View>

                        <View style={[styles.fieldView, { marginVertical: 5 }]}>
                            <View style={styles.cardColumn}>
                                <Text style={[
                                    styles.textStyle,
                                    { width: this.getTextWidth(20) }]}>
                                    21/11/1988
                                </Text>
                            </View>
                            <View style={styles.hintLine} />
                            <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Fecha de Nacimiento</Text>
                        </View>

                    </View>
                </View >
                < View style={styles.cardContainer} >
                    <View style={styles.fieldView}>
                        <View style={styles.cardColumn}>
                            <Text style={[
                                styles.textStyle,
                                { width: this.getTextWidth(80) }]}>
                                No configurado
                                </Text>
                        </View>
                        <View style={styles.hintLine} />
                        <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Tipo de cliente</Text>
                    </View>

                    <View style={styles.fieldView}>
                        <View style={styles.cardColumn}>
                            <Text style={[
                                styles.textStyle,
                                { width: this.getTextWidth(102) }]}>
                                CLV
                                </Text>
                        </View>
                        <View style={styles.hintLine} />
                        <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Coupon Code</Text>
                    </View>

                    <View style={styles.fieldView}>
                        <View style={styles.cardColumn}>
                            <Text style={[
                                styles.textStyle,
                                { width: this.getTextWidth(86) }]}>
                                CVL121331B00
                                </Text>
                        </View>
                        <View style={styles.hintLine} />
                        <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Coupon History</Text>
                    </View>
                </View>
                < View style={styles.cardContainer} >
                    <View style={styles.fieldView}>
                        <View style={styles.cardColumn}>
                            <Text style={[
                                styles.textStyle,
                                { width: this.getTextWidth(80) }]}>
                                30-09-2016
                                </Text>
                        </View>
                        <View style={styles.hintLine} />
                        <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Fecha Alta</Text>
                    </View>

                    <View style={styles.fieldView}>
                        <View style={styles.cardColumn}>
                            <Text style={[
                                styles.textStyle,
                                { width: this.getTextWidth(102) }]}>
                                CLV
                                </Text>
                        </View>
                        <View style={styles.hintLine} />
                        <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Coupon Code</Text>
                    </View>

                    <View style={styles.fieldView}>
                        <View style={styles.cardColumn}>
                            <Text style={[
                                styles.textStyle,
                                { width: this.getTextWidth(86) }]}>
                                CVL121331B00
                                </Text>
                        </View>
                        <View style={styles.hintLine} />
                        <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Coupon History</Text>
                    </View>
                </View>
                < View style={styles.cardContainer} >
                    <View style={styles.fieldView}>
                        <View style={styles.cardColumn}>
                            <Text style={[
                                styles.textStyle,
                                { width: this.getTextWidth(21) }]}>
                                Not Set
                            </Text>
                        </View>
                        <View style={styles.hintLine} />
                        <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>IBAN</Text>
                    </View>

                    <View style={styles.fieldView}>
                        <View style={styles.cardColumn}>
                            <Text style={[
                                styles.textStyle,
                                { width: this.getTextWidth(21) }]}>
                                Not Set
                            </Text>
                        </View>
                        <View style={styles.hintLine} />
                        <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>IBAN2</Text>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        let deviceWidth = Dimensions.get('window').width;
        return (
            <View style={styles.screenContainer} >
                <ScrollView>
                    {this.renderHeader()}
                    {this.renderClientDetails()}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    boxWithShadow: {
        alignSelf: 'flex-end',
        margin: 8,
        marginRight: 12,
        borderRadius: 25,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.ibanOnlineGreen,
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    rowText: {
        fontSize: 20
    },
    screenContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    textStyle: {
        fontSize: 16,
        marginBottom: 0
    },
    headerTextStyle: {
        flex: 2,
        fontSize: 12,
        paddingLeft: 8,
        marginTop: 2,
        marginStart: 4,
        marginBottom: 0
    },
    searchContainerStyle: {
        backgroundColor: 'white',
        borderTopWidth: 0,
        borderBottomWidth: 0
    },
    searchInputStyle: {
        color: 'black',
        borderRadius: 6
    },
    separator: {
        marginBottom: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#dcdcdc'
    }, cardContainer: {
        marginVertical: 5,
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    cardColumnContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    cardContainerVertical: {
        // borderWidth: 1,
        // borderColor: 'green',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
    },
    cardColumn: {
        flexDirection: 'row',
        marginTop: 2,
        marginBottom: 0
    },
    icon: {
        padding: 2,
        marginLeft: 13

    },
    searchIcon: {
        marginRight: 5
    },
    cardText: {
        padding: 1
    },
    hintLine: {
        height: 0.7,
        backgroundColor: 'gray'
    },
    hintTextStyle: {
        marginLeft: 0,
        marginTop: 5,
        fontSize: 14,
        color: Colors.ibanOnlineGreen
    },

    fieldView: {
        marginLeft: 10,
        marginRight: 10
    },
    bottomTextStyle: {
        marginBottom: 5
    },
});