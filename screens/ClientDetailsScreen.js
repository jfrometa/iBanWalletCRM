import React, { Component } from 'react';
import {
    Dimensions,
    Image,
    ListView,
    PixelRatio,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import Colors from '../constants/Colors';
import ClientTransactionCard from '../components/ClientTransactionCard'
import ClientTransactionRow from '../components/ClientTransactionRow'

class ClientDetailsScreen extends Component {
    static navigationOptions = {
        title: 'Detalles',
    };
    constructor(props) {
        super(props);
        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            }).cloneWithRows([
                'Simplicity Matters',
                'Hammock Driven Development',
            ])
        };
    }

    componentWillMount() {
        this.createDataSource(this.dummyData);
    }

    getTextWidth(substract) {
        return (Dimensions.get('window').width / 2) - substract
    }

    getHeight(num) {
        // console.log(Dimensions.get('window').height)
        return (Dimensions.get('window').height / num);
    }

    createDataSource() {
        dummyData = [
            "title 0", "title 1", "title 2", "title 3", "title 4", "title 6", "title 7",
            "title 8", "title 9", "title 10", "title 11", "title 12", "title 13"
        ];
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(dummyData);
    }

    renderHeader() {
        const { navigate } = this.props.navigation;
        return (
            <View key="fixed-header"
                style={{ backgroundColor: 'white', paddingTop: 8, paddingBottom: 8 }}>
                <View style={{ flexDirection: 'column' }}>
                    <View
                        style={[styles.boxWithShadow, styles.shadow,
                        { width: this.getTextWidth(152) }]}>
                        <Text
                            style={[
                                {
                                    margin: 0,
                                    padding: 0,
                                    textAlign: 'center',
                                    fontSize: 14,
                                    fontWeight: '500',
                                    color: 'white',
                                    alignSelf: 'center'
                                }]}
                            onPress={() => navigate('MoreDetails')}
                        >Otros
                        </Text>
                    </View>
                    <Text
                        style={[styles.headerTextStyle, {
                            textAlign: 'center',
                            fontSize: 20,
                            fontWeight: '500',
                            marginTop: 30
                        }]}>
                        Jose Joaquin Frometa Guerra
                    </Text>
                    <Text
                        style={[styles.headerTextStyle,
                        {
                            textAlign: 'center',
                            fontSize: 14,
                            fontWeight: '500',
                            color: 'gray'
                        }]}
                    >CLV507536</Text>

                </View>

            </View>
        );

    }

    renderClientDetails() {
        return (
            < View key="sticky-header" style={styles.cardContainerVertical} >
                <View style={[styles.fieldView, { marginVertical: 5 }]}>
                    <View style={styles.cardColumn}>
                        <Text style={[
                            styles.textStyle,
                            { width: this.getTextWidth(20) }]}>
                            Jfrometa@gmail.com
                        </Text>
                    </View>
                    <View style={styles.hintLine} />
                    <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Correo Electronico</Text>
                </View>
                < View style={styles.cardContainer} >
                    <View style={styles.cardColumnContainer}>
                        <View style={[styles.fieldView, { marginVertical: 5 }]}>
                            <View style={styles.cardColumn}>
                                <Text style={[
                                    styles.textStyle,
                                    {
                                        width: this.getTextWidth(20)
                                    }]}>Marjolaine Fages
                            </Text>
                            </View>
                            <View style={styles.hintLine} />
                            <Text style={[styles.hintTextStyle,
                            styles.bottomTextStyle]}>Gestor
                                </Text>
                        </View>
                        <View style={[styles.fieldView, { marginVertical: 5 }]}>
                            <View style={styles.cardColumn}>
                                <Text style={[
                                    styles.textStyle,
                                    { width: this.getTextWidth(20) }]}>
                                    +1 809 988 - 0271
                                </Text>
                            </View>
                            <View style={styles.hintLine} />
                            <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Telefono</Text>
                        </View>
                    </View>
                    <View style={styles.cardColumnContainer}>
                        <View style={[styles.fieldView, { marginVertical: 5 }]}>
                            <View style={styles.cardColumn}>
                                <Text style={[
                                    styles.textStyle,
                                    { width: this.getTextWidth(20) }]}>
                                    1.046,86 EUR
                                </Text>
                            </View>
                            <View style={styles.hintLine} />
                            <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Balance</Text>
                        </View>

                        <View style={[styles.fieldView, { marginVertical: 5 }]}>
                            <View style={styles.cardColumn}>
                                <Text style={[
                                    styles.textStyle,
                                    { width: this.getTextWidth(20) }]}>
                                    Restringido
                                </Text>
                            </View>
                            <View style={styles.hintLine} />
                            <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>T. Inversor</Text>
                        </View>
                    </View>
                </View >
            </ View >
        );
    }

    renderProductDetails() {
        return (
            <ClientTransactionCard>
                <ListView
                    enableEmptySections
                    dataSource={this.dataSource}
                    renderRow={this.renderRow.bind(this)}
                    renderSeparator={(sectionID, rowID) =>
                        <View key={`${sectionID}-${rowID}`} style={styles.separator} />
                    }
                />
            </ClientTransactionCard>
        );
    }

    renderRow(transactionDetails) {
        return (
            <ClientTransactionRow />
        );
    }

    render() {
        const { onScroll = () => { } } = this.props;
        return (
            <ListView
                ref="ListView"
                style={styles.container}
                dataSource={this.state.dataSource}
                renderRow={(rowData) => (this.renderProductDetails())}
                renderScrollComponent={props => (
                    <ParallaxScrollView
                        onScroll={onScroll}
                        backgroundColor='white'
                        headerBackgroundColor='white'
                        //stickyHeaderHeight={STICKY_HEADER_HEIGHT}
                        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
                        backgroundSpeed={10}

                        renderBackground={() => (
                            <View key="background"
                                style={{ marginTop: 120 }}>
                                {this.renderClientDetails()}
                            </View>
                        )}
                        // renderForeground={() => (this.renderHeader())}
                        // renderStickyHeader={() => (this.renderClientDetails())}
                        renderFixedHeader={() => (this.renderHeader())} />
                )}
            />
        );
    }
}
/* <Text style={styles.fixedSectionText}
    onPress={() => this.refs.ListView.scrollTo({ x: 0, y: 0 })}>
    Scroll to top
    </Text> */
const window = Dimensions.get('window');
const PARALLAX_HEADER_HEIGHT = 300;
const STICKY_HEADER_HEIGHT = 150;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    boxWithShadow: {
        alignSelf: 'flex-end',
        margin: 8,
        marginRight: 12,
        borderRadius: 25,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.ibanOnlineGreen,
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    background: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: window.width,
        height: PARALLAX_HEADER_HEIGHT
    },
    stickySection: {
        height: STICKY_HEADER_HEIGHT,
        width: 300,
        justifyContent: 'flex-end'
    },
    stickySectionText: {
        color: 'white',
        fontSize: 20,
        margin: 10
    },
    fixedSection: {
        position: 'absolute',
        bottom: 10,
        right: 10
    },
    fixedSectionText: {
        color: '#999',
        fontSize: 20
    },
    parallaxHeader: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'column',
        paddingTop: 100
    },
    sectionSpeakerText: {
        color: 'white',
        fontSize: 24,
        paddingVertical: 5
    },
    sectionTitleText: {
        color: 'white',
        fontSize: 18,
        paddingVertical: 5
    },
    // row: {
    //     overflow: 'hidden',
    //     paddingHorizontal: 10,
    //     height: ROW_HEIGHT,
    //     backgroundColor: 'white',
    //     borderColor: '#ccc',
    //     borderBottomWidth: 1,
    //     justifyContent: 'center'
    // },
    rowText: {
        fontSize: 20
    },
    screenContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    textStyle: {
        fontSize: 16,
        marginBottom: 0
    },
    headerTextStyle: {
        flex: 2,
        fontSize: 12,
        paddingLeft: 8,
        marginTop: 2,
        marginStart: 4,
        marginBottom: 0
    },
    searchContainerStyle: {
        backgroundColor: 'white',
        borderTopWidth: 0,
        borderBottomWidth: 0
    },
    searchInputStyle: {
        color: 'black',
        borderRadius: 6
    },
    separator: {
        marginBottom: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#dcdcdc'
    }, cardContainer: {
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    cardColumnContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    cardContainerVertical: {
        // borderWidth: 1,
        // borderColor: 'green',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
    },
    cardColumn: {
        flexDirection: 'row',
        marginTop: 2,
        marginBottom: 0
    },
    icon: {
        padding: 2,
        marginLeft: 13

    },
    searchIcon: {
        marginRight: 5
    },
    cardText: {
        padding: 1
    },
    hintLine: {
        height: 0.7,
        backgroundColor: 'gray'
    },
    hintTextStyle: {
        marginLeft: 0,
        marginTop: 5,
        fontSize: 14,
        color: Colors.ibanOnlineGreen
    },

    fieldView: {
        marginLeft: 10,
        marginRight: 10
    },
    bottomTextStyle: {
        marginBottom: 5
    },
});

export default ClientDetailsScreen;