import React, {Component} from 'react'
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { Ionicons} from '@expo/vector-icons';
import SimplePicker from 'react-native-simple-picker';
import Colors from '../constants/Colors'

const styles = StyleSheet.create({
    container: {
        margin:4,
        marginTop: 8,
        justifyContent: 'center',
        backgroundColor: Colors.foreGroundDarkGray,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    icon: {
        marginRight: 5
    },
    shadow:{
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    }
});


export default class Spinner extends Component {
    constructor(props) {
        super(props);

        this.icons = {
            'up': 'ios-arrow-up',
            'down': 'ios-arrow-down'
        };

        this.state = {
            selectedOption: '',
            open:false
        };
    }

    render() {
        let icon = this.icons['down'];

        if (this.state.open) {
            icon = this.icons['up'];
        }

        return (
            <View style={[styles.container, styles.shadow]}>

                <View style ={{ 
                    margin: 2,
                    flexDirection: 'row'
                }}>
                    <Text
                        style={{ 
                            height:30,
                            textAlign:'center',
                            justifyContent: 'center',
                            paddingTop: 5.5,
                            color: Colors.textColor,
                            width: this.props.width 
                        }}
                        onPress={() => {
                            this.refs.picker.show();
                            this.setState({
                                open:true
                            })
                        }}
                    >
                        {this.state.selectedOption ? this.state.selectedOption : this.props.title + ' :'}
                    </Text>

                    <Ionicons
                        style={styles.icon}
                        name={icon} size={16}
                        color={Colors.ibanOnlineGreen} />
                </View>


                <SimplePicker
                    ref={'picker'}
                    options={this.props.options}
                    modalStyle={{padding:20, backgroundColor: 'white'}}
                    itemStyle={{
                        fontSize: 15,
                        color: 'black',
                        textAlign: 'center',
                        fontWeight: 'bold',
                    }}
                    onSubmit={(option) => {
                        this.setState({
                            selectedOption: option,
                            open: false
                        });
                    }}
                    onCancel={() => {
                        this.setState({
                            open: false
                        });
                    }}
                />

            </View>
        );
    }
}