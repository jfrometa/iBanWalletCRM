import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    Animated,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';
import { SearchBar } from 'react-native-elements'
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { Akira, Hoshi, Makiko } from 'react-native-textinput-effects';
import Spinner from './Spinner'
import DatePicker from './DatePicker'
import InputText from './InputText'
import Colors from '../constants/Colors';

class SearchAndFilter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
            animation: new Animated.Value(),
            startAmount: 0,
            endAmount:0,
            startDate:'',
            endDate:''
        };
    }

    componentDidMount() {
        // this.setState({
        //     expanded: !this.state.expanded
        // });
    }

    toggle() {
        let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue = this.state.expanded ? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

        this.setState({
            expanded: !this.state.expanded
        });

        this.state.animation.setValue(initialValue);
        Animated.spring(
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();
    }

    _setMaxHeight(event) {
        this.setState({
            maxHeight: event.nativeEvent.layout.height
        });
    }

    _setMinHeight(event) {
        this.setState({
            minHeight: event.nativeEvent.layout.height
        });
    }

    getTextWidth(substract) {
        // get the dimensions for diferent phone sizes
        let deviceWidth = Dimensions.get('window').width;
        return (deviceWidth / 2) - substract
    }

    renderClientCardBody() {
        products = ['Todos', 'Cuenta', 'Uno', 'Mercado', 'Inversor'];
        currency = ['EUR', 'GBP', 'USD']

        let deviceWidth = Dimensions.get('window').width;

        if (this.state.expanded) {
            return (
                <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>

                    <View style={{flexDirection: 'row', justifyContent: 'center',alignItems: 'center'}}>
                        <Spinner 
                            options={products}
                            width={this.getTextWidth(40)}
                            title={"Producto"}
                            />
                        <Spinner 
                            options={currency}
                            width={this.getTextWidth(40)}
                            title={"Moneda"}
                            />
                    </View>
                    <View style={{ flexDirection: 'row', margin: 6,
                          justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{
                            justifyContent: 'center',
                            marginTop: 6,
                            fontSize: 15,
                            marginLeft: 2,
                            color: Colors.textColor
                        }}>
                            Saldo: </Text>
                        <InputText
                            label={'Desde'}
                            iconClass={MaterialIcons}
                            iconName={'attach-money'}
                            iconColor={'gray'}
                            keyboardType="numeric"
                            style={{ width: this.getTextWidth(45) }}
                            inputStyle={{ fontSize: 15 }}
                        />

                        <Text style={{
                            justifyContent: 'center',
                            marginTop: 5, 
                            fontSize: 15,
                            color:Colors.textColor}}> a </Text>
                        

                        <InputText
                            label={'Hasta'}
                            iconClass={MaterialIcons}
                            iconName={'attach-money'}
                            iconColor={'gray'}
                            keyboardType="numeric"
                            style={{ width: this.getTextWidth(45) }}
                            inputStyle={{ fontSize: 15, margin: 0, padding: 0 }}
                        />
                    </View>
                    <DatePicker style={{ 
                        backgroundColor: Colors.backGroundDarkGray}}/>
                    <View 
                        style={[styles.buttonWithShadow, 
                            { width: this.getTextWidth(-130), 
                            backgroundColor:Colors.ibanOnlineGreen }]}>
                        <TouchableHighlight
                            style={styles.button}
                            onPress={this.toggle.bind(this)}
                            underlayColor="rgba(255, 255, 255, .4)">
                        <Text 
                            style= {{
                            color: 'white', 
                            fontWeight: '400', 
                            fontSize: 16}}
                        > 
                            Filtrar 
                        </Text>
                        </TouchableHighlight>
                    </View>
            </View>
        );
        } else {
            return (
                <View />
            );
        }
    }

    renderSearchBar() {
        //get the screen width
        let deviceWidth = Dimensions.get('window').width;

        return (
            <View style={styles.searchViewContainer}>
                <SearchBar
                    // onChangeText={someMethod}
                    // onClear={someMethod}
                    lightTheme
                    cancelButtonTitle='Cancel'
                    platform='ios'
                    containerStyle={
                        [styles.searchContainerStyle,
                        { width: deviceWidth - 35 }]
                    }
                    icon={{ type: 'font-awesome', 
                            name: 'search', 
                            color: Colors.ibanOnlineGreen   
                    }}
                    inputStyle={styles.searchInputStyle}
                    placeholder=' ' />

                <View style={styles.filterButton}>
                    <TouchableHighlight
                        onPress={this.toggle.bind(this)}
                        underlayColor="#f1f1f1">
                    <FontAwesome
                        style={styles.icon}
                        name={"filter"} size={20}
                        color="gray" />

                </TouchableHighlight>
                </View>
            </View>
        )
    }

    render() {

        return (
            <Animated.View
                style={[styles.container, { height: this.state.animation }]}>
                {this.renderSearchBar()}
                {this.renderClientCardBody()}
            </Animated.View>
        );
    }
}
// 
var styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff'
    },
    shadow:{
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    titleContainer: {
        flex: 1,
        padding: 5,
        flexDirection: 'row',
    },
    title: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    clvStyle: {
        alignSelf: 'center',
        fontSize: 12,
        color: 'blue'
    },
    currencyStyle: {
        fontSize: 12
    },
    button: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonWithShadow:{
        height: 35,
        margin: 8,
        marginBottom: 16,
        backgroundColor: Colors.foreGroundDarkGray,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        alignSelf: 'center',
    },
    currencyAndIconStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    body: {
        backgroundColor: Colors.backGroundDarkGray
    }, 
    icon: {
        padding: 2
    },
    cardColumn: {
        flexDirection: 'row',
        margin: 5,
        marginBottom: 0
    }, 
    filterButton: {
        width: 40,
        paddingEnd: 6,
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center'
    },
    searchViewContainer: {
        flexDirection: 'row',
        borderColor: 'white',
        justifyContent: 'flex-start'
    },
    searchContainerStyle: {
        backgroundColor: 'white',
        borderTopWidth: 0,
        borderBottomWidth: 0
    },
    searchInputStyle: {
        backgroundColor: "#f6f6f6",
        color: 'black',
        borderRadius: 6
    }
});

export default SearchAndFilter