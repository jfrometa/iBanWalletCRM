import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,

} from 'react-native';
import Colors from '../constants/Colors';


export default class DisplayProductWithIdBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hasProducts: false,
            productName: '',
            productId: ''
        };
    }

    componentDidMount() {
        if (this.props.hasProducts) {
            this.setState({
                hasProducts: true,
                productId: this.props.productId,
                productName: this.props.productName
            });
        }
    }

    getTextWidth(substract) {
        // get the dimensions for diferent phone sizes
        let deviceWidth = Dimensions.get('window').width;
        return (deviceWidth / 2) - substract
    }

    renderProductAndID() {
        if (this.state.hasProducts) {
            return (`${this.state.productName} - ${this.state.productId}`);
        } else {
            return 'No Tiene Productos';
        }

    }

    render() {
        return (
            <View
                style={[styles.boxWithShadow,
                {
                    width: this.getTextWidth(20),
                    backgroundColor: 'white'
                }]}>
                <Text
                    style={{
                        color: 'gray',
                        fontWeight: '400',
                        fontSize: 16
                    }}
                >
                    {this.renderProductAndID()}
                </Text>

            </View>
        );
    }

}


var styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff'
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    titleContainer: {
        flex: 1,
        padding: 5,
        flexDirection: 'row',
    },
    title: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    clvStyle: {
        alignSelf: 'center',
        fontSize: 12,
        color: 'blue'
    },
    currencyStyle: {
        fontSize: 12
    },
    button: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxWithShadow: {
        height: 35,
        margin: 8,
        borderRadius: 25,
        flexDirection: 'column',
        alignItems: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.foreGroundDarkGray,
        alignSelf: 'center',
    },
    currencyAndIconStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    body: {
        backgroundColor: Colors.backGroundDarkGray
    },
    icon: {
        padding: 2
    },
    cardColumn: {
        flexDirection: 'row',
        margin: 5,
        marginBottom: 0
    },
    filterButton: {
        width: 40,
        paddingEnd: 6,
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center'
    },
    searchViewContainer: {
        flexDirection: 'row',
        borderColor: 'white',
        justifyContent: 'flex-start'
    },
    searchContainerStyle: {
        backgroundColor: 'white',
        borderTopWidth: 0,
        borderBottomWidth: 0
    },
    searchInputStyle: {
        backgroundColor: "#f6f6f6",
        color: 'black',
        borderRadius: 6
    }
});