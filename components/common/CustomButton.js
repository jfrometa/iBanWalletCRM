import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Colors from '../../constants/Colors'

const CustomButton = ({ onPress, children }) => {
  const { buttonStyle, textStyle, shadow } = styles;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[buttonStyle, shadow]}
    >
      <Text style={textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  shadow: {
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },
  textStyle: {
    textDecorationLine: 'none',
    alignSelf: 'center',
    color: Colors.ibanOnlineGreen,
    fontSize: 16,
    fontWeight: '500',
    paddingTop: 10,
    paddingBottom: 10
  },
  buttonStyle: {
    marginTop: 5,
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.ibanOnlineGreen,
    marginRight: 5,
    marginLeft: 5
  }
};

export { CustomButton };
