import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { C_DEFAULT_TEXT_COLOR, C_NAV_MENU, C_GREY_2, C_GREY_1 } from '../../actions/types';

const styles = StyleSheet.create({
  viewStyle: {
    height: 30,
    flexDirection: 'column',
    backgroundColor: C_NAV_MENU,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyleTName: {
    textAlign: 'center',
    alignSelf: 'center',
    fontSize: 15,
    fontWeight: '400',
    color: C_GREY_2
  },
  textStyleDName: {
    textAlign: 'center',
    alignSelf: 'center',
    fontSize: 15,
    fontWeight: '400',
    color: C_GREY_1
  }
});
//make a Component
const TournamentsLabel = props => {
  //declare the styles object
  const { viewStyle, textStyleTName, textStyleDName } = styles;

  return (
    <View style={viewStyle}>
      <Text style={textStyleTName}>{props.tournamentLabel}</Text>
      <Text style={textStyleDName}>{props.divisionLabel}</Text>
    </View>
  );
};
//make the component available to other parts of the App
//export { TournamentsLabel };
export default TournamentsLabel;
