import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { C_DEFAULT_TEXT_COLOR, C_NAV_MENU } from '../../actions/types';

const styles = StyleSheet.create({
  viewStyle: {
    height: 140,
    flexDirection: 'row',
    backgroundColor: C_NAV_MENU,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 25,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2
  },
  textStyle: {
    textAlign: 'center',
    width: 150,
    alignSelf: 'center',
    fontSize: 17,
    color: C_DEFAULT_TEXT_COLOR
  },
  twoDotsStyle: {
    marginTop: 16,
    fontSize: 20,
    color: C_DEFAULT_TEXT_COLOR
  },
  image: {
    width: 50,
    height: 50
  }
});
//make a Component
const Header = props => {
  //declare the styles object
  const { viewStyle, textStyle, twoDotsStyle } = styles;

  return (
    <View style={viewStyle}>
      <View style={{ flexDirection: 'column', alignItems: 'center' }}>
        <Text style={textStyle}> {props.team_a_name} </Text>
        <Image
          style={styles.image}
          source={{ uri: `${props.team_a_image}` }}
          resizeMode="contain"
          cache
        />
      </View>
      <Text style={twoDotsStyle}>
        {props.team_a_score} : {props.team_b_score}
      </Text>
      <View style={{ flexDirection: 'column', alignItems: 'center' }}>
        <Text style={textStyle}> {props.team_b_name} </Text>
        <Image
          style={styles.image}
          source={{ uri: `${props.team_b_image}` }}
          resizeMode="contain"
          cache
        />
      </View>
    </View>
  );
};
//make the component available to other parts of the App
export { Header };
