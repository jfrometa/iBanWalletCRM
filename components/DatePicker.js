import React, { Component } from 'react'
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableWithoutFeedback,
    Dimensions
} from 'react-native';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import BaseCalendarPicker from './BaseCalendarPicker';
import Colors from '../constants/Colors'

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    paragraph: {
        textAlign: 'center',
        color: '#002f2f',
        marginBottom: 5,
        fontWeight: 'bold',
        fontSize: 18,
    },
    icon: {
        marginRight: 5
    }
});


export default class DatePicker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedOption: '',
            selectedStartDate: 'Fecha Inicial',
            selectedEndDate: 'Fecha Final',
            open: false
            // options: props.options
        };

    }

    getTextWidth(substract) {
        return (Dimensions.get('window').width - substract)
    }

    render() {
        return (
            <View style={[styles.container, this.props.style]}>
                <TouchableWithoutFeedback
                    style={{ flex: 1 }}
                    onPress={() => {
                        this.refs.datepicker.show();
                    }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'center',
                                margin: 2,
                                borderBottomColor: 'grey',
                                width: 40
                            }}>

                            <FontAwesome
                                style={styles.icon}
                                name={"calendar"} size={20}
                                color={Colors.ibanOnlineGreen} />
                        </View>
                        <View style={{
                            width: this.getTextWidth(60),
                            justifyContent: 'center'
                        }}>
                            <Text style={{ margin: 5, color: 'black', textAlign: 'center' }}>
                                {`${this.state.selectedStartDate} - ${this.state.selectedEndDate}`}
                            </Text>
                            <View style={{ height: 1, backgroundColor: 'grey', marginBottom: 4 }} />
                        </View >
                    </View>
                </TouchableWithoutFeedback>
                <BaseCalendarPicker
                    ref={'datepicker'}
                    onSubmit={(startDate, endDate) => {
                        this.setState({
                            selectedStartDate: startDate,
                            selectedEndDate: endDate,
                            open: false
                        });
                    }}
                    onCancel={() => {
                        this.setState({
                            open: false
                        });
                    }}
                />
            </View>
        );
    }
}