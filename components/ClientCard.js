import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    Animated,
    Dimensions,
} from 'react-native';
import { Ionicons, MaterialCommunityIcons, EvilIcons } from '@expo/vector-icons';
import Colors from '../constants/Colors'

const theme = React.createContext({})

class ClientCard extends Component {
    constructor(props) {
        super(props);

        this.icons = {
            'up': "ios-arrow-up",
            'down': "ios-arrow-down"
        };

        this.state = {
            title: props.title,
            expanded: false,
            animation: new Animated.Value()
        };
    }



    componentWillMount() {
        //console.log(theme)
        // this.setState({
        //     expanded: !this.state.expanded
        // });
    }

    toggle() {
        let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue = this.state.expanded ? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

        this.setState({
            expanded: !this.state.expanded
        });

        this.state.animation.setValue(initialValue);
        Animated.spring(
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();
    }

    _setMaxHeight(event) {
        this.setState({
            maxHeight: event.nativeEvent.layout.height
        });
    }

    _setMinHeight(event) {
        this.setState({
            minHeight: event.nativeEvent.layout.height
        });
    }

    adjustComponentWidth(substract) {
        let deviceWidth = Dimensions.get('window').width;
        return (deviceWidth / substract);

    }

    renderClientCardBody() {
        if (this.state.expanded) {
            return (
                <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
                    {this.props.children}
                </View>);
        } else {
            return (
                <View />
            );
        }
    }

    renderClientCardHeader(icon) {

        return (
            <View style={styles.titleContainer}
                onLayout={this._setMinHeight.bind(this)}>
                <View
                    style={{
                        width: this.adjustComponentWidth(3.9),
                        flexDirection: 'column', padding: 4
                    }}>
                    <EvilIcons
                        style={{ alignSelf: 'center' }}
                        name={"user"} size={48} color="grey" />

                    <Text
                        style={styles.clvStyle}
                    >CLV507536</Text>
                </View>

                <View style={{
                    width: this.adjustComponentWidth(1.45),
                    padding: 4, position: 'relative'
                }}>
                    <Text style={styles.title}
                    >Jose Joaquin Frometa Guerra
                    </Text>

                    <View style={{ flexDirection: 'row', left: 9, marginTop: 5 }}>
                        <View style={styles.cardColumn}>
                            <MaterialCommunityIcons
                                name={"heart-box"}
                                size={19}
                                color={Colors.ibanOnlineGreen} />
                            <Text
                                style={[styles.textStyle,
                                { color: 'gray', marginRight: 0 }]}
                            >Mercado
                            </Text>
                        </View>

                        <View style={styles.cardColumn}>
                            <MaterialCommunityIcons name={"currency-eur"}
                                size={18} color={Colors.ibanOnlineGreen} />
                            <Text style={[styles.textStyle, { color: 'gray' }]}>1.785,00</Text>
                        </View>
                    </View>
                </View>
                <Ionicons
                    style={[styles.icon]}
                    name={icon} size={15}
                    color={Colors.iconColor} />
            </View>);
    }

    render() {
        let icon = this.icons['down'];

        if (this.state.expanded) {
            icon = this.icons['up'];
        }

        return (
            <Animated.View
                style={[styles.container, { height: this.state.animation }]}>
                <TouchableHighlight
                    style={styles.button}
                    onPress={this.toggle.bind(this)}
                    underlayColor="rgba(255, 255, 255, .4)">

                    {this.renderClientCardHeader(icon)}

                </TouchableHighlight>

                {this.renderClientCardBody()}
            </Animated.View>
        );
    }
}


var styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        margin: 1,
    },
    titleContainer: {
        flexDirection: 'row',
    },
    title: {
        textAlign: 'left',
        left: 15,
        marginTop: 5,
        fontSize: 16,
        fontWeight: '400'
    },
    textStyle: {
        fontSize: 15,
    },
    clvStyle: {
        padding: 2,
        marginTop: 5,
        alignSelf: 'center',
        fontSize: 14,
        color: 'black',
        fontWeight: '400'
    },
    button: {
        flexDirection: 'column',
    },
    cardColumn: {
        flexDirection: 'row',
        margin: 5,
        marginBottom: 0
    },
    icon: {
        margin: 2,
        marginTop: 5,
        marginRight: 6,
    }
});

export default ClientCard