import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    Animated,
    Dimensions,
} from 'react-native';
import { Ionicons, MaterialCommunityIcons, EvilIcons } from '@expo/vector-icons';
import Colors from '../constants/Colors'
import DisplayProductWithIdBox from '../components/DisplayProductWithIdBox'

class ClientTransactionCard extends Component {
    constructor(props) {
        super(props);

        this.icons = {
            'up': "ios-arrow-up",
            'down': "ios-arrow-down"
        };

        this.state = {
            balance: '1.046,86 EUR',
            expanded: false,
            animation: new Animated.Value()
        };
    }

    componentDidMount() {
        // this.setState({
        //     expanded: !this.state.expanded
        // });
    }

    toggle() {
        let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue = this.state.expanded ? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

        this.setState({
            expanded: !this.state.expanded
        });

        this.state.animation.setValue(initialValue);
        Animated.spring(
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();
    }

    _setMaxHeight(event) {
        this.setState({
            maxHeight: event.nativeEvent.layout.height
        });
    }

    _setMinHeight(event) {
        this.setState({
            minHeight: event.nativeEvent.layout.height
        });
    }

    adjustComponentWidth(substract) {
        let deviceWidth = Dimensions.get('window').width;
        return (deviceWidth / substract);

    }

    renderTransactionCardBody() {
        if (this.state.expanded) {
            return (
                <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
                    {this.props.children}
                </View>);
        } else {
            return (
                <View />
            );
        }
    }

    renderTransactionCardHeader(icon) {

        return (
            <View style={styles.cardHeaderContainer}
                onLayout={this._setMinHeight.bind(this)}>

                <DisplayProductWithIdBox
                    hasProducts={true}
                    productId='CLV632677'
                    productName='Cuenta'
                />

                <View style={{
                    width: this.adjustComponentWidth(2.2),
                    padding: 4, position: 'relative',
                    alignSelf: 'center'
                }}>

                    <Text style={[
                        styles.balanceTextStyle,]}>
                        {`Balance: ${this.state.balance}`}
                    </Text>

                </View>
                <Ionicons
                    style={[styles.icon]}
                    name={icon} size={16}
                    color={'white'} />
            </View>);
    }

    render() {
        let icon = this.icons['down'];

        if (this.state.expanded) {
            icon = this.icons['up'];
        }

        return (
            <Animated.View
                style={[styles.container, styles.shadow, { height: this.state.animation }]}>
                <TouchableHighlight
                    style={styles.button}
                    onPress={this.toggle.bind(this)}
                    underlayColor='rgba(0, 0, 0, .29)'>

                    {this.renderTransactionCardHeader(icon)}

                </TouchableHighlight>

                {this.renderTransactionCardBody()}
            </Animated.View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        margin: 1,
    },
    cardHeaderContainer: {
        backgroundColor: Colors.ibanOnlineGreen,
        flexDirection: 'row',
    },
    shadow: {
        backgroundColor: Colors.foreGroundDarkGray,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    title: {
        textAlign: 'left',
        left: 15,
        fontSize: 14,
        fontWeight: '400'
    },
    balanceTextStyle: {
        color: 'white',
        fontSize: 16
    },
    currencyStyle: {
        marginTop: 5,
        fontSize: 12
    },
    button: {
        flexDirection: 'column',
    },
    currencyAndIconStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    body: {

    },
    cardColumn: {
        flexDirection: 'row',
        margin: 5,
        marginBottom: 0
    },
    icon: {
        margin: 2,
        marginTop: 5,
        marginRight: 6,
    }
});

export default ClientTransactionCard