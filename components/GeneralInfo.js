import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    Animated,
    Dimensions,
    Platform
} from 'react-native';
import {
    Ionicons,
    MaterialIcons,
    Entypo
} from '@expo/vector-icons';
import Colors from '../constants/Colors'

class GeneralInfo extends Component {
    constructor(props) {
        super(props);

        this.icons = {
            'up': "ios-arrow-up",
            'down': "ios-arrow-down"
        };

        this.state = {
            nextExpirations: 0,
            currentBalance: 0,
            yesterdaysMov: 0,
            lastSevenMov: 0,
        };
    }

    getTextWidth(substract) {
        // get the dimensions for diferent phone sizes
        let deviceWidth = Dimensions.get('window').width;
        return (deviceWidth / 2) - substract
    }

    formatCurrency(num) {
        if (Platform.OS === 'ios') {
            return (new Intl.
                NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).
                format(num)
            );
        } else {
            return (num);
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.fieldView, { marginTop: 5 }]}>
                    <View style={styles.cardColumn}>
                        <Text style={[
                            styles.textStyle,
                            { width: this.getTextWidth(40) }]}>
                            {this.formatCurrency(1000000)}</Text>
                        <Entypo style={styles.icon} name={"wallet"} size={12} color="gray" />
                    </View>
                    <View style={styles.hintLine} />
                    <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Saldo Actual</Text>

                    <View style={styles.cardColumn}>
                        <Text style={[
                            styles.textStyle,
                            { width: this.getTextWidth(40) }]}>
                            {this.formatCurrency(2000000)}</Text>
                        <MaterialIcons style={styles.icon} name={"attach-money"} size={12} color="gray" />
                    </View>
                    <View style={styles.hintLine} />
                    <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Mov Desde ayer</Text>
                </View>
                <View style={[styles.fieldView, { marginTop: 5 }]}>
                    <View style={styles.cardColumn}>
                        <Text style={[
                            styles.textStyle,
                            { width: this.getTextWidth(40) }]}>
                            {this.formatCurrency(3000000)}</Text>
                        <MaterialIcons style={styles.icon} name={"attach-money"} size={12} color="gray" />
                    </View>
                    <View style={styles.hintLine} />
                    <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Mov. 7 Dias</Text>

                    <View style={styles.cardColumn}>
                        <Text style={[
                            styles.textStyle,
                            { width: this.getTextWidth(40) }]}>112</Text>
                        <Ionicons style={styles.icon} name={"ios-speedometer"} size={12} color="gray" />
                    </View>
                    <View style={styles.hintLine} />
                    <Text style={[styles.hintTextStyle, styles.bottomTextStyle]}>Proximos vencimientos</Text>
                </View>
            </View>
        );
    }

}
var styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    fieldView: {
        margin: 10
    },
    titleContainer: {
        flex: 1,
        padding: 5,
        flexDirection: 'row',
    },
    title: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    clvStyle: {
        alignSelf: 'center',
        fontSize: 12,
        color: 'blue'
    },
    textStyle: {
        width: 100,
        fontSize: 16,
        color: 'gray'
    },
    bottomTextStyle: {
        marginBottom: 5
    },
    hintLine: {
        height: 2,
        backgroundColor: 'grey'
    },
    hintTextStyle: {
        marginLeft: 0,
        marginTop: 5,
        fontSize: 14,
        color: Colors.ibanOnlineGreen
    },
    button: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        margin: 2
    },
    icon: {
        height: 15,
        width: 15,
        marginTop: 5,
        marginLeft: 5
    },
    currencyAndIconStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    body: {
        padding: 10,
        paddingTop: 0
    },
    cardColumn: {
        flexDirection: 'row',
        marginTop: 6,
        marginLeft: 0,
        marginBottom: 0
    }
});
export default GeneralInfo;


