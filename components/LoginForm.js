import React, { Component } from 'react';
import { View, Image, Text, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { SocialIcon } from 'react-native-elements';
import { CardSection, Input, Spinner, CustomButton } from './common';
import { COMPANY_LOGO } from '../actions/types';
import {
  emailChanged,
  passwordChanged,
  loginUser,
  facebookbLogin,
  googleLogin
} from '../actions';


class LoginForm extends Component {
  componentDidMount() {
    //this.onGoogleLogin();
    //this.onFacebookLogin();
  }
  componentWillReceiveProps(nextProps) {
    this.onAuthComplete(nextProps);
  }

  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }
  onButtonPress() {
    const { email, password } = this.props;
    this.props.loginUser({ email, password });
  }

  onFacebookLogin() {
    this.props.facebookbLogin(() => {
      //Actions.main();
      Actions.bottomTabBar({ type: 'reset' });
      //Actions.caltab();
      //      Actions.calendar();
    });
  }

  onGoogleLogin() {
    this.props.googleLogin(() => {
      //Actions.main();
      Actions.bottomTabBar({ type: 'reset' });
      //Actions.caltab();
      //      Actions.calendar();
    });
  }

  onAuthComplete(props) {
    if (props.token) {
      //Actions.main();
    }
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
    return (
      <CustomButton onPress={this.onButtonPress.bind(this)} >
        Login
    </CustomButton>
    );
  }

  render() {
    // AsyncStorage.removeItem('fb_token');
    // AsyncStorage.removeItem('google_token');
    return (
      <View style={{ flex: 1 }}>

        <Image
          style={{ width: 240, height: 240, alignSelf: 'center', marginTop: 40 }}
          resizeMode='cover'
          source={COMPANY_LOGO}
        />

        <View>
          <CardSection>
            <Input
              label="Email"
              placeholder="email@gmail.com"
              onChangeText={this.onEmailChange.bind(this)}
              value={this.props.email}
            />
          </CardSection>

          <CardSection>
            <Input
              secureTextEntry
              label="Password"
              placeholder="Password"
              onChangeText={this.onPasswordChange.bind(this)}
              value={this.props.password}
            />
          </CardSection>
          <Text style={styles.errorTextStyle}>
            {this.props.error}
          </Text>
        </View>
        <View>
          <CardSection>
            {this.renderButton()}
          </CardSection>
        </View>
        <View>
          <SocialIcon
            onPress={() => this.onFacebookLogin()}
            title='Sign In With Facebook'
            button
            raised
            type='facebook'
          />
          <SocialIcon
            onPress={() => this.onGoogleLogin()}
            title='Sign In With Google'
            button
            type='google-plus-official'
          />
        </View>
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};
//mapstatetoprops = (state) and state constins auth (state.auth)
//auth contains the others from our reducer
const mapStateToProps = ({ auth }) => {
  const { email, password, error, loading, token } = auth;
  return {
    email,
    password,
    loading,
    error,
    token
  };
};

export default connect(mapStateToProps,
  {
    emailChanged,
    passwordChanged,
    loginUser,
    facebookbLogin,
    googleLogin
  })(LoginForm);
