import React, { Component } from 'react'; // eslint-disable-line

import PropTypes from 'prop-types';

import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Modal,
    Picker,
    Dimensions,
    TouchableWithoutFeedback,
} from 'react-native'; // eslint-disable-line
import moment from 'moment'
import CalendarPicker from 'react-native-calendar-picker';
import Colors from '../constants/Colors'

const SCREEN_WIDTH = Dimensions.get('window').width;

const styles = {
    basicContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    overlayContainer: {
        flex: 1,
        width: SCREEN_WIDTH,
    },

    mainBox: {
        // Can be used by <CalendarPicker styles={{ mainBox:{...} }}/>
    },

    modalContainer: {
        width: SCREEN_WIDTH,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        backgroundColor: 'white',
    },

    buttonView: {
        width: SCREEN_WIDTH,
        padding: 8,
        borderTopWidth: 0.5,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },

    bottomPicker: {
        width: SCREEN_WIDTH,
    },
};

const propTypes = {
    buttonColor: PropTypes.string,
    buttonStyle: PropTypes.object,
    cancelText: PropTypes.string,
    confirmText: PropTypes.string,
    disableOverlay: PropTypes.bool,
    initialOptionIndex: PropTypes.number,
    itemStyle: PropTypes.object,
    labels: PropTypes.array,
    modalVisible: PropTypes.bool,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    styles: PropTypes.object,
};

const booleanIsSet = variable => variable || String(variable) === 'false';

class BaseCalendarPicker extends Component {
    constructor(props) {
        super(props);

        const selected = props.initialOptionIndex || 0;

        this.state = {
            modalVisible: props.modalVisible || false,

            selectedOption: '',
            selectedStartDate: '',
            selectedEndDate: '',

        };

        this.styles = StyleSheet.create({
            ...styles,
            ...props.styles,
        });


        this.onDateChange = this.onDateChange.bind(this);
        this.onPressCancel = this.onPressCancel.bind(this);
        this.onPressSubmit = this.onPressSubmit.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onOverlayDismiss = this.onOverlayDismiss.bind(this);
    }


    onDateChange(date, type) {
        if (type === 'END_DATE') {
            this.setState({
                selectedEndDate: moment(date, 'DD-MM-YYYY')
                    .format().substring(10, 0),
            });
        } else {
            this.setState({
                selectedStartDate: moment(date, 'DD-MM-YYYY')
                    .format().substring(10, 0),
                selectedEndDate: 'Fecha Final',
            });
        }
    }

    componentWillReceiveProps(props) {

        // Options array changed and the previously selected option is not present anymore.
        // Should call onSubmit function to tell parent to handle the change too.

        //this.onPressSubmit();

        if (booleanIsSet(props.modalVisible)) {
            this.setState({
                modalVisible: props.modalVisible,
            });
        }
    }

    onPressCancel() {
        if (this.props.onCancel) {
            this.props.onCancel(this.state.selectedOption);
        }

        this.hide();
    }

    onPressSubmit() {
        if (this.props.onSubmit) {
            this.props.onSubmit(
                this.state.selectedStartDate.toString() ?
                    this.state.selectedStartDate.toString() : 'Fecha Inicial',
                this.state.selectedEndDate.toString()) ?
                this.state.selectedEndDate.toString() : 'Fecha Final'
        }

        this.hide();
    }

    onOverlayDismiss() {
        if (this.props.onCancel) {
            this.props.onCancel(this.state.selectedOption);
        }

        this.hide();
    }

    onValueChange(option) {
        this.setState({
            selectedOption: option,
        });
    }

    show() {
        this.setState({
            modalVisible: true,
        });
    }

    hide() {
        this.setState({
            modalVisible: false,
        });
    }

    renderItem(option, index) {
        const label = (this.props.labels) ? this.props.labels[index] : option;

        return (
            <BaseCalendarPicker.Item
                key={option}
                value={option}
                label={label}
            />
        );
    }

    render() {
        const { modalVisible, selectedOption } = this.state;
        const {
			options,
            buttonStyle,
            itemStyle,
            cancelText,
            confirmText,
            disableOverlay,
        } = this.props;

        const { selectedStartDate, selectedEndDate } = this.state;
        const minDate = new Date(2015, 1, 1); // Today
        const maxDate = new Date(2030, 31, 12);
        const startDate = selectedStartDate ? selectedStartDate.toString() : '';
        const endDate = selectedEndDate ? selectedEndDate.toString() : '';


        return (
            <Modal
                animationType={'slide'}
                transparent
                visible={modalVisible}
            >
                <View style={this.styles.basicContainer}>
                    {!disableOverlay &&
                        <View style={this.styles.overlayContainer}>
                            <TouchableWithoutFeedback onPress={this.onOverlayDismiss}>
                                <View style={this.styles.overlayContainer} />
                            </TouchableWithoutFeedback>
                        </View>
                    }
                    <View style={this.styles.modalContainer}>
                        <View style={this.styles.buttonView}>
                            <TouchableOpacity onPress={this.onPressCancel}>
                                <Text style={buttonStyle}>
                                    {cancelText || 'Cancelar'}
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={this.onPressSubmit}>
                                <Text style={buttonStyle}>
                                    {confirmText || 'Confirmar'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={this.styles.mainBox}>
                            <CalendarPicker
                                startFromMonday={true}
                                allowRangeSelection={true}
                                minDate={minDate}
                                maxDate={maxDate}
                                todayBackgroundColor="#f2e6ff"
                                selectedDayColor="#7300e6"
                                selectedDayTextColor="#FFFFFF"
                                onDateChange={this.onDateChange}
                            />
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

BaseCalendarPicker.defaultProps = {
    styles: {},
};

BaseCalendarPicker.propTypes = propTypes;

module.exports = BaseCalendarPicker;