
import React, { Component } from 'react';
import { View, Text, AsyncStorage, Image, Dimensions, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { Card, CardSection, Input, CustomButton, Spinner } from './common';
import {
  emailChanged,
  passwordChanged,
  loginUser,
  facebookbLogin
} from '../actions';

const COMPANY_LOGO = require('../assets/images/splash.png');

class Login extends Component {
  static navigationOptions = {
    title: 'Login',
  };

  componentDidMount() {
    // this.onFacebookLogin();
  }
  componentWillReceiveProps(nextProps) {
    //  this.onAuthComplete(nextProps);
  }

  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }
  onButtonPress() {
    //const { email, password } = this.props;
    const { navigate } = this.props.navigation;
    navigate('Main')
    //  this.props.loginUser({ email, password });
  }

  onFacebookLogin() {
    this.props.facebookbLogin(() => {
      //Actions.main();
      // Actions.bottomTabBar({ type: 'reset' });
      // Actions.caltab();
      //      Actions.calendar();
    });
  }

  onAuthComplete(props) {
    if (props.token) {
      //  Actions.pop();
    }
  }

  getTextWidth(substract) {
    return (Dimensions.get('window').width / 2) - substract
  }

  getHeight(num) {
    // console.log(Dimensions.get('window').height)
    return (Dimensions.get('window').height / num);
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
    return (
      <CustomButton onPress={this.onButtonPress.bind(this)}>
        Login
    </CustomButton>
    );
  }

  render() {
    //AsyncStorage.removeItem('fb_token');
    return (
      <ScrollView>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{
          marginVertical: this.getHeight(4.9),
          backgroundColor: 'white'
        }}>
          <Image
            style={{
              resizeMode: 'center',
              backgroundColor: 'white',
              alignSelf: 'center'
            }}
            source={COMPANY_LOGO}
          />
        </View>
        <CardSection>
          <Input
            label="Email"
            placeholder="email@gmail.com"
            onChangeText={this.onEmailChange.bind(this)}
            value={this.props.email}
          />
        </CardSection>

        <CardSection>
          <Input
            secureTextEntry
            label="Password"
            placeholder="Password"
            onChangeText={this.onPasswordChange.bind(this)}
            value={this.props.password}
          />
        </CardSection>

        <Text style={styles.errorTextStyle}>
          {this.props.error}
        </Text>


        <CustomButton onPress={this.onButtonPress.bind(this)}>
          Login
        </CustomButton>

      </View>
      </ScrollView>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};
//mapstatetoprops = (state) and state constins auth (state.auth)
//auth contains the others from our reducer
const mapStateToProps = ({ auth }) => {
  const { email, password, error, loading, token } = auth;
  return {
    email,
    password,
    loading,
    error,
    token
  };
};

export default connect(mapStateToProps,
  {
    emailChanged,
    passwordChanged,
    loginUser,
    facebookbLogin
  })(Login);
