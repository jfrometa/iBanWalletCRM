import _ from 'lodash';
import React, { Component } from 'react';
import {
    ListView,
    View,
    Text,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
} from 'react-native';
import { Ionicons, EvilIcons, Entypo, MaterialIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import Colors from '../constants/Colors';
import ClientTransactionCard from '../components/ClientTransactionCard'

export default class ClientDetailsScreen extends View {
    constructor() {
        super();

        this.state = {
            date: '',
            type: '',
            transacValue: '',
            balance: '',
            currency: '',
            description: ''
        }
    }



    getTextWidth(substract) {
        // get the dimensions for diferent phone sizes
        let deviceWidth = Dimensions.get('window').width;
        return (deviceWidth / 2) - substract
    }
    //

    renderDescription() {
        if (this.state.description !== '')
            return (
                <View style={[
                    {
                        margin: 4,
                        marginLeft: 16,
                        marginBottom: 4,
                    }]}>
                    <Text style={{ textAlign: 'left' }}>
                        {` Descipcion: ${this.state.description}`}
                    </Text>

                </View >
            );
    }

    renderClientDetails() {
        return (
            < View style={styles.cardContainerVertical} >
                <View style={[styles.fieldView, { flexDirection: 'row' }]}>

                    <View style={[styles.cardColumn, { flex: 2 }]}>
                        <MaterialIcons style={styles.icon}
                            name={"swap-vert"} size={18} color={Colors.ibanOnlineGreen} />
                        <Text style={styles.textStyle}>1000</Text>
                    </View>

                    <View style={[styles.cardColumn, { flex: 2 }]}>
                        <MaterialIcons style={styles.icon}
                            name={"swap-horiz"} size={18} color={Colors.ibanOnlineGreen} />
                        <Text style={[styles.textStyle, { paddingLeft: 0 }]}>Transfer en línea - OPI</Text>
                    </View>

                </View>

                <View style={[styles.fieldView, { flexDirection: 'row' }]}>


                    <View style={[styles.cardColumn, { flex: 2 }]}>
                        <MaterialIcons style={styles.icon}
                            name={"account-balance"} size={18} color={Colors.ibanOnlineGreen} />
                        <Text style={styles.textStyle}>10500</Text>
                    </View>

                    <View style={[styles.cardColumn, { flex: 2 }]}>
                        <Ionicons style={styles.icon} name={"ios-calendar"} size={18} color={Colors.ibanOnlineGreen} />
                        <Text style={styles.textStyle}>05-12-2016 (15:43:35)</Text>
                    </View>

                </View>



                {this.renderDescription()}

            </View >
        );
    }



    render() {
        //get the screen width
        let deviceWidth = Dimensions.get('window').width;
        return (
            <View style={styles.screenContainer} >
                {this.renderClientDetails()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screenContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    textStyle: {
        fontSize: 13,
        margin: 2,
        marginBottom: 0
    },
    headerTextStyle: {
        flex: 2,
        fontSize: 12,
        paddingLeft: 8,
        marginTop: 2,
        marginStart: 4,
        marginBottom: 0
    },
    searchContainerStyle: {
        backgroundColor: 'white',
        borderTopWidth: 0,
        borderBottomWidth: 0
    },
    searchInputStyle: {
        color: 'black',
        borderRadius: 6
    },
    separator: {
        marginBottom: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#dcdcdc'
    }, cardContainer: {
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    cardColumnContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    cardContainerVertical: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: 'white',
    },
    cardColumn: {
        flexDirection: 'row',
        margin: 4,
        marginBottom: 0
    },
    icon: {
        padding: 2,
        marginLeft: 0

    },
    searchIcon: {
        marginRight: 5
    },
    cardText: {
        padding: 1
    },
    hintLine: {
        height: 2,
        backgroundColor: 'grey'
    },
    hintTextStyle: {
        marginLeft: 0,
        marginTop: 5,
        fontSize: 12,
        color: Colors.ibanOnlineGreen
    },

    fieldView: {
        marginLeft: 10,
        marginRight: 10
    },
    bottomTextStyle: {
        marginBottom: 5
    },
});