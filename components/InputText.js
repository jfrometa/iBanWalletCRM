import React from 'react';
import PropTypes from 'prop-types';
import {
    Animated,
    Easing,
    Text,
    TextInput,
    TouchableWithoutFeedback,
    View,
    StyleSheet,
} from 'react-native';

import BaseInput from './BaseInput';
import Colors from '../constants/Colors';

const PADDING = 8;

export default class InputText extends BaseInput {
    static propTypes = {
        /*
         * This is the icon component you are importing from react-native-vector-icons.
         * import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
         * iconClass={FontAwesomeIcon}
         */
        iconClass: PropTypes.func.isRequired,

        /*
         * Passed to react-native-vector-icons library as name prop.
         * This icon expands and covers the input.
         * So, the icon should not have any blank spaces for animation experience.
         * This is the limitation for Makiko.
         */
        iconName: PropTypes.string.isRequired,

        /*
         * Passed to react-native-vector-icons library as color prop
         */
        iconColor: PropTypes.string,

        /*
         * Use iconSize and iconWidth to make the animation work for your icon
         */
        iconSize: PropTypes.number,
        iconWidth: PropTypes.number,
    };

    static defaultProps = {
        iconColor: 'gray',
        iconSize: 15,
        iconWidth: 15,
        height: 40,
        easing: Easing.bezier(0.7, 0, 0.3, 1),
        animationDuration: 300,
    };

    render() {
        const {
            iconClass,
            iconColor,
            iconName,
            iconSize,
            iconWidth,
            style: containerStyle,
            height: inputHeight,
            inputStyle,
            label,
            labelStyle,
    } = this.props;
        const {
      width,
            focusedAnim,
            value,
    } = this.state;
        const AnimatedIcon = Animated.createAnimatedComponent(iconClass);

        return (
            <View
                style={[styles.container, containerStyle]}
                onLayout={this._onLayout}
            >
                <TouchableWithoutFeedback onPress={this.focus}>
                    <View
                        style={{
                            position: 'absolute',
                            height: inputHeight,
                            width,
                        }}
                    >
                        <Text
                            style={[
                                styles.label,
                                labelStyle,
                                { left: iconWidth, color: iconColor },
                            ]}
                        >
                            {label}
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <Animated.View
                    style={{
                        position: 'absolute',
                        backgroundColor: Colors.backGroundDarkGray,
                        left: 0,
                        height: inputHeight ,
                        width: focusedAnim.interpolate({
                            inputRange: [0, 0.2, 1],
                            outputRange: [0, 0, width],
                        }),
                    }}
                />
                <TextInput
                    ref="input"
                    {...this.props}
                    style={[
                        styles.textInput,
                        inputStyle,
                        {
                            width,
                            height: inputHeight,
                        },
                    ]}
                    value={value}
                    onBlur={this._onBlur}
                    onChange={this._onChange}
                    onFocus={this._onFocus}
                    underlineColorAndroid={'transparent'}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        height:30,
        backgroundColor: 'white',
        overflow: 'hidden',
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    label: {
        paddingVertical: 10,
        paddingHorizontal: 19,
        position: 'relative',
        fontSize: 15,
        color: 'gray',
        backgroundColor: 'transparent',
    },
    textInput: {
        paddingVertical: PADDING,
        paddingHorizontal: PADDING,
        paddingVertical: 0,
        textAlign: 'center',
        color: Colors.textColor,
        fontSize: 18,
    },
});