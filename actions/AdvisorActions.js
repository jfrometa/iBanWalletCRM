import { AsyncStorage } from 'react-native';
import {
    SEARCH_ADVISOR,
    CLEAN_ADVISOR,
    FILTER_ADVISOR,
    CLEAN_FILTER_ADVISOR
} from '../constants/Types'


export const findAdvisor = (str) => {
    return dispatch => {
        dispatch({ type: SEARCH_ADVISOR, payload: str });
    }
}

export const cleanAdvisor = () => {
    return dispatch => {
        dispatch({ type: CLEAN_ADVISOR });
    }
}

export const filterAdvisors = (product, currency, initBalance, endBalance, startDate, endDate) => {
    const filter = {
        product,
        currency,
        initBalance,
        endBalance,
        startDate,
        endDate
    }

    return dispatch => {
        dispatch({ type: FILTER_ADVISOR, payload: filter });
    }
}

export const cleanFilterAdvisors = () => {
    return dispatch => {
        dispatch({ type: CLEAN_FILTER_ADVISOR });
    }
}
