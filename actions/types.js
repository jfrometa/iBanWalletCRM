export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USERS_SUCCESS = 'login_user_success';
export const LOGIN_USERS_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';

//facebook
export const FB_LOGIN = 'fb_login';
export const FB_LOGIN_SUCCESS = 'fb_login_success';
export const FB_LOGIN_FAIL = 'fb_login_fail';

//Google
export const GOOGLE_LOGIN = 'google_login';
export const GOOGLE_LOGIN_SUCCESS = 'google_login_success';
export const GOOGLE_LOGIN_FAIL = 'google_login_fail';

//Calendar
export const REQUEST_CALENDAR = 'request_calendar';
export const REQUEST_CALENDAR_SUCCESS = 'request_calendar_success';
export const REQUEST_CALENDAR_FAIL = 'request_calendar_fail';
export const TOGGLE_SLIDE_UP_PANEL = 'toggle_slide_up_panel';

export const REQUEST_TOURNAMENT_DIVISIONS = 'request_tournament_divivions';
export const REQUEST_TOURNAMENT_DIVISIONS_SUCCESS = 'request_tournament_divivions_success';
export const REQUEST_TOURNAMENT_DIVISIONS_FAIL = 'request_tournament_divivions-fail';

//divisions

export const REQUEST_DIVISIONS = 'request_divisions';
export const REQUEST_DIVISIONS_SUCCESS = 'request_divisions_success';
export const SELECTED_DIVISION = 'selected_division';
export const SELECTED_DIVISION_NAME = 'selected_division_name';

//FB Request

export const REQUEST_FB_POST = 'request_posts';
export const REQUEST_FB_POST_SUCCESS = 'request_posts_success';
export const REQUEST_FB_SPONSOR_SUCCESS = 'request_sponsor_success';
export const REQUEST_FB_POST_FAIL = 'request_posts_fail';
export const SELECTED_POST = 'selected_post';

//tournament

export const REQUEST_TOURNAMENT = 'request_tournament';
export const REQUEST_TOURNAMENT_SUCCESS = 'request_tournament_success';
export const SELECTED_TOURNAMENT = 'selected_tournament';
export const SELECTED_TOURNAMENT_NAME = 'selected_tournament_name';

//Match Details

export const REQUEST_MVP_FOR_MATCH = 'request_mvp_for_match';
export const REQUEST_MVP_FOR_MATCH_SUCCESS = 'request_mvp_for_match_success';
export const REQUEST_MVP_FOR_MATCH_FAIL = 'request_mvp_for_match_fail';
export const REQUEST_MATCH_DETAILS = 'request_match_details';
export const REQUEST_MATCH_DETAILS_SUCCESS = 'request_match_details_success';
export const UPDATE_MATCH_DETAILS_ID_A = 'update_match_details_id_a';
export const UPDATE_MATCH_DETAILS_ID_B = 'update_match_details_id_b';

// Stats Table
export const REQUEST_STATS_TABLE = 'request_stats_table';
export const REQUEST_STATS_TABLE_SUCCESS = 'request_stats_table_success';
export const REQUEST_STATS_TABLE_FAIL = 'request_stats_table_fail';

// Stats Table
export const REQUEST_LEADERS = 'request_leaders';
export const REQUEST_LEADERS_SUCCESS = 'request_leaders_success';
export const REQUEST_LEADERS_FAIL = 'request_leaders_fail';
export const REQUEST_GOAL_LEADERS_SUCCESS = 'request_goal_leaders_success';
export const REQUEST_GOAL_LEADERS_FAIL = 'request_goal_leaders_fail';
export const REQUEST_YELLOW_CARD_LEADERS_SUCCESS = 'request_yellow_card_leaders_success';
export const REQUEST_YELLOW_CARD_LEADERS_FAIL = 'request_yellow_card_leaders_fail';
export const REQUEST_RED_CARD_LEADERS_SUCCESS = 'request_red_card_leaders_success';
export const REQUEST_RED_CARD_LEADERS_FAIL = 'request_red_card_leaders_fail';

//FACEBOOK PAGE ID
export const COMPANY_MAIN_FACEBOOK = '296124937084496';
export const COMPANY_SPONSOR_FACEBOOK = '153032708103061';
/////COLORS

export const C_PRIMARY_DARK = '#242424';
export const C_PRIMARY = '#332E29';
export const C_PRIMARY_LIGHT = '#4d4d4d';
export const C_CARD_COLOR = '#FAFAFA';
export const C_NEWS_GRADIENT = '#C0C0C0';
export const C_NAV_MENU = '#FFFFFF';
export const C_DEFAULT = '#757575';
export const C_BOTTOMBAR = '#FAFAFA';
export const C_DEFAULT_TEXT_COLOR = '#333333';
export const C_SPONSOR_1 = '#1B2E68';

// ACCENT COLORS
//export const C_ACCENT = '#3dbc26';
export const C_ACCENT = '#4CD964';
export const C_MATCH_DETAILS_LINEA = '#83997e';
export const C_ACCENT_DARK = '#8c8906';
export const C_ACCENT_LIGHT = '#58eaa1';

//OTHERS
export const C_UPGRADE_ORANGE = '#ff3a1d';
export const C_UPGRADE = '#FF3A1D';
export const C_GREY_100 = '#F5F5F5';
export const C_GREY_300 = '#E0E0E0';
export const C_GREY_1 = '#B6B6B6';
export const C_GREY_2 = '#767676';
export const C_GREY_3 = '#DCDCDC';
export const C_BLACK_1 = '#373737';
export const C_BLACK_OVERLAY = '#660000';
export const C_BLACK_0 = '#1e1e1e';
export const C_BLACK_TEXT = '#000000';
export const C_DIVIDER = '#dddddd';

// // // IMAGES
// export const GOAL_IMAGE = require('../assets/goal.png');
// export const TOURNAMENT_IMAGE = require('../assets/Torneo.png');
// export const YELLOW_CARD_IMAGE = require('../assets/yellow_card.png');
// export const RED_CARD_IMAGE = require('../assets/red_card.png');
// export const TIME_IMAGE = require('../assets/time.jpeg');
// export const I_LEADS = require('../assets/lideres.png');
// export const I_NEWSFEED = require('../assets/Newsfeed.png');
// export const I_CALENDAR = require('../assets/Calendario.png');
// export const COMPANY_LOGO = require('../assets/app_icon.png');
// export const BPD_LOGO = require('../assets/bpd_logo_horizontal.png');
// export const UPGRADE_LOGO = require('../assets/Upgrade.png');
// export const SETTINGS = require('../assets/Settings.png');

// ////// ICONS
// export const ICONS = {
//   user: require('../assets/Imissing_player.png'),
//   club: require('../assets/Iclubs.png'),
//   division: require('../assets/Idivisions.png'),
//   goals: require('../assets/Igoals.png'),
//   cards: require('../assets/Icards.png'),
//   position: require('../assets/Iposition.png'),
//   dominantFoot: require('../assets/Idominantfoot.png'),
//   weight: require('../assets/Iweight.png'),
//   mvp: require('../assets/mvp.png')
// };
