import { AsyncStorage } from 'react-native';
import { Facebook, Google } from 'expo';
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USERS_SUCCESS,
  LOGIN_USERS_FAIL,
  LOGIN_USER,
  FB_LOGIN,
  FB_LOGIN_SUCCESS,
  FB_LOGIN_FAIL,
  GOOGLE_LOGIN,
  GOOGLE_LOGIN_SUCCESS,
  GOOGLE_LOGIN_FAIL
} from './types';

export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

/// EMAIL AND PASSWORD ACTION CREATORS
export const loginUser = ({ email, password }) => {
  //redux-thunk calls the dispatch func to dispatch de action for our reducers
  return (dispatch) => {
    dispatch({ type: LOGIN_USER });

    // firebase.auth().signInWithEmailAndPassword(email, password)
    // .then(user => loginUserSuccess(dispatch, user))
    // .catch(() => {
    //   firebase.auth().createUserWithEmailAndPassword(email, password)
    //     .then(user => loginUserSuccess(dispatch, user))
    //     .catch(err => loginUserFail(dispatch, err));
    //   });
  };
};

const loginUserFail = (dispatch, err) => {
  // console.log(err);
  dispatch({ type: LOGIN_USERS_FAIL, payload: err });
};

const loginUserSuccess = (dispatch, user) => {
  dispatch({ type: LOGIN_USERS_SUCCESS, payload: user });
  //  Actions.bottomTabBar({ type: 'reset' });
};

/// FACEBOOK LOGIN USER CREATORS
export const facebookbLogin = (callBack) => async dispatch => {
  dispatch({ type: FB_LOGIN });
  // const token = await AsyncStorage.getItem('fb_token');
  // if (token) {
  //   dispatch(fbLoginSuccess(dispatch, token, callBack));
  // } else {
  //     doFacebookLogin(dispatch, callBack);
  // }
};

const doFacebookLogin = async (dispatch, callBack) => {
  //   try {
  //     const { type, token } = await Facebook.logInWithReadPermissionsAsync(
  //       '116280978808946', {
  //       permissions: ['public_profile']
  //     });
  //     if (type === 'cancel') {
  //       fbLoginFail(dispatch);
  //     }
  //   if (token != null) {
  //       await AsyncStorage.setItem('fb_token', token);
  //       if (type === 'success') {
  //   // Get the user's name using Facebook's Graph API

  // }
  //       const credential = firebase.auth.FacebookAuthProvider.credential(token);
  //       firebase
  //         .auth()
  //         .signInWithCredential(credential)
  //         .then(() => console.log('Account accepted'))
  //         .catch(() => fbLoginFail(dispatch));
  //     } else {
  //       console.log(`${token} Account disabled`);
  //       fbLoginFail(dispatch);
  //       return;
  //     }
  //     fbLoginSuccess(dispatch, token, callBack);
  //   } catch (e) {
  //     fbLoginFail(dispatch);
  //   }
};

const fbLoginFail = (dispatch) => {
  return dispatch({ type: FB_LOGIN_FAIL });
};

const fbLoginSuccess = (dispatch, token, callBack) => {
  dispatch({ type: FB_LOGIN_SUCCESS, payload: token });
  callBack();
};


//// GOOGLE LOGIN ACTION CREATORS
export const googleLogin = (callBack) => async dispatch => {
  dispatch({ type: GOOGLE_LOGIN });
  // const token = await AsyncStorage.getItem('google_token');
  // if (token) {
  //   dispatch(googleLoginSuccess(dispatch, token, callBack));
  //   //Actions.bottomTabBar({ type: 'reset' });
  // } else {
  //     doGoogleLogin(dispatch, callBack);
  // }
};


const doGoogleLogin = async (dispatch, callBack) => {
  // try {
  // const { type, accessToken } = await Google.logInAsync({
  //     iosClientId: '643327074360-s3iblq45vlpbc536rdf5ojkv1qe43gu6.apps.googleusercontent.com',
  //     scopes: ['profile', 'email']
  // });
  // console.log(type);
  // if (type === 'cancel') {
  //   googleLoginFail(dispatch);
  // }
  // if (accessToken != null) {
  //     await AsyncStorage.setItem('google_token', accessToken);
  //     const credential = firebase.auth.GoogleAuthProvider.credential(accessToken);
  //     firebase
  //       .auth()
  //       .signInWithCredential(credential)
  //       .then(() => console.log('Account accepted'))
  //       .catch(() => googleLoginFail(dispatch));
  //   } else {
  //     console.log(`${accessToken} Account disabled`);
  //     googleLoginFail(dispatch);
  //     return;
  //   }
  //   googleLoginSuccess(dispatch, accessToken, callBack);
  // } catch (e) {
  //   googleLoginFail(dispatch);
  // }
};

const googleLoginFail = (dispatch) => {
  return dispatch({ type: GOOGLE_LOGIN_FAIL });
};

const googleLoginSuccess = (dispatch, token, callBack) => {
  dispatch({ type: GOOGLE_LOGIN_SUCCESS, payload: token });
  callBack();
};
