import React from 'react';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TabNavigator, TabBarBottom } from 'react-navigation';

import Colors from '../constants/Colors';

import ClientDetailsScreen from '../screens/ClientDetailsScreen';
import ClientListScreen from '../screens/ClientListScreen';
import SettingsScreen from '../screens/SettingsScreen';
import ClientMoreDetailsScreen from '../screens/ClientMoreDetailsScreen';
import Login from '../components/Login';

export default TabNavigator(
  {
    Asesores: {
      screen: ClientListScreen,
    },

    // MoreDetails: {
    //   screen: ClientMoreDetailsScreen,
    // },
    // Clientes: {
    //   screen: ClientDetailsScreen,
    // },
    Settings: {
      screen: SettingsScreen,
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarOptions: {
        activeTintColor: 'black',
      },
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'Clientes':
            iconName =
              Platform.OS === 'ios'
                ? `ios-person${focused ? '' : '-outline'}`
                : 'ios-person';
            break;
          case 'Asesores':
            iconName = Platform.OS === 'ios' ? `ios-people${focused ? '' : '-outline'}` : 'ios-people';
            break;
          case 'Settings':
            iconName =
              Platform.OS === 'ios' ? `ios-options${focused ? '' : '-outline'}` : 'md-options';
        }
        return (
          <Ionicons
            name={iconName}
            size={28}
            style={{ marginBottom: -3, width: 25 }}
            color={focused ? Colors.backGroundDarkGray : Colors.tabIconDefault}
          />
        );
      },
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: false,
  }
);
